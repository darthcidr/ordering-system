//initializes semantic dropdown
$('.ui.dropdown').not('.ui.simple.dropdown.item').dropdown();
//initializes rating
$('.ui.star.rating').rating('disable');
$('#feedback-satisfaction-rating').val($('#satisfaction-rating').rating('get rating'));
$('#satisfaction-rating').rating({
	onRate: function() {
		$('#feedback-satisfaction-rating').val($('#satisfaction-rating').rating('get rating'));
		// console.log($('#feedback-satisfaction-rating').rating('get rating'));
	}
});
//initialize semantic ui dropdown
$('.ui.accordion').accordion();

//smooth scrolling...is annie okay?
$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	// On-page links
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		// Figure out element to scroll to
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		// Does a scroll target exist?
		if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1000);
		}
	}
});

//toastr option configuration
toastr.options = {
	'closeButton': true,
	'debug': false,
	'newestOnTop': true,
	'progressBar': true,
	'positionClass': 'toast-top-right',
	'preventDuplicates': false,
	'onclick': null,
	'showDuration': '300',
	'hideDuration': '1000',
	'timeOut': '7500',
	'extendedTimeOut': '1000',
	'showEasing': 'swing',
	'hideEasing': 'linear',
	'showMethod': 'fadeIn',
	'hideMethod': 'fadeOut'
}

// //nanobar.init
// var nanobar_conf = {
// 	classname: 'my-class',
// 	id: 'my-id',
// 	target: document.getElementById('myDivid')
// };
// var nanobar = new Nanobar(nanobar_conf);

//set number of buttons in data table pagination
// $.fn.DataTable.ext.pager.numbers_length = 5;

function arrSum(array) {
	var sum = 0;
	for (var i = 0; i < array.length; i++) {
		if (typeof array[i]['mealprice'] == 'object') {
			sum += arrSum((parseFloat(array[i]['mealprice']) * parseFloat(array[i]['mealqty'])));
		}
		else {
			sum += (parseFloat(array[i]['mealprice']) * parseFloat(array[i]['mealqty']));
		}
	}
	return sum;
}
//get meals for dropdown in order form
$.ajax({
	url: '/lists/meals',
	method: 'get',
	cache: false,
	dataType: 'json',
	success: function (data) {
		if (data != undefined) {
			var array = [];
			$.each(data, function(i,val){
				array.push('<option value = "'+val.id+'" data-price = "'+val.price+'">'+val.desc+' (PHP '+val.price+')</option>');
			});
			$('#cmb-order-meals').html(array);
		}
		else {
			toastr.error('Error loading meals.');
		}
	}
});

//order placement mechanism
var toorder = [];
var tempID = 0;
$('#tblMealsToOrder tfoot').on('click', '#btnAddMealToOrder', function(){
	$('#tblMealsToOrder tbody').empty();
	var mealid 			= $(this).closest('tr').find('#cmb-order-meals option:selected').val();
	var mealdesc		= $(this).closest('tr').find('#cmb-order-meals option:selected').text();
	var mealprice		= $(this).closest('tr').find('#cmb-order-meals option:selected').data('price');
	var mealqty			= $(this).closest('tr').find('#txt-order-qty').val();
	
	tempID++;
	selected = {
		'tempID'	: tempID,
		'mealid'	: mealid,
		'mealdesc'	: mealdesc,
		'mealprice'	: mealprice,
		'mealqty'	: mealqty
	};
	toorder.push(selected);
	$.each(toorder, function(i,val){
		$('#tblMealsToOrder tbody').append(
			'<tr>'+
				'<td>'+val.tempID+'</td>'+
				'<td>'+val.mealdesc+'</td>'+
				'<td>'+val.mealqty+'</td>'+
				'<td><button type = "button" id = "btnRemoveMealToOrder" class = "ui mini button"><i class = "trash icon"></i> Remove from cart</button></td>'+
			'</tr>'
		);
	});
	console.log(toorder);
	console.log(arrSum(toorder));
	$('#nud-grand-total').val(arrSum(toorder));

	//reset everything
	$(this).closest('tr').find('#cmbMarketCollectionServices').dropdown('restore defaults');
	$(this).closest('tr').find('#nudMarketCollectionCost').val('');
});
//delete appended row to tblMealsToOrder
$('#tblMealsToOrder tbody').on('click','#btnRemoveMealToOrder', function(event){
	var todelete = $(this).closest('tr').remove();
	$.each(toorder,function(i,val){
		if (toorder[i].tempID == val.tempID) {
			toorder.splice(i,1);
			return false;
		}
	});
	$('#nud-grand-total').val(arrSum(toorder));
	// toassign.splice(id,1);
});
$('#frm-order').submit(function(event){
	event.preventDefault();
	// nanobar.go(80);
	$('#btn-order, #div-order-form').toggleClass('loading',true);
	$.ajax({
		url: '/orders/order-placement/new',
		method: 'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: {
			'token'						: $('#token').val(),
			'txt-order-fname'			: $('#txt-order-fname').val(),
			'txt-order-lname'			: $('#txt-order-lname').val(),
			'txt-order-contact-number'	: $('#txt-order-contact-number').val(),
			'txt-order-address'			: $('#txt-order-address').val(),
			'txt-order-notes'			: $('#txt-order-notes').val(),
			'nud-grand-total'			: $('#nud-grand-total').val(),
			'toorder'					: toorder
		},
		dataType: 'json',
		beforeSend: function(xhr) {
			if (toorder.length == 0) {
				xhr.abort();
				toastr.error('Please order a meal first.');
			}
		},
		success: function (result) {
			if (result["saved"] == true) {
				// toorder = [];
				// $('#tblMealsToOrder tbody').empty();
				// $('.ui.search.dropdown').dropdown('restore defaults');
				// $('#frm-order')[0].reset();

				// toastr.success(result["success"]);
				// window.open('/reports/receipt/order/'+result["order_id"]);
				self.location = '/orders/order-placement/'+result["order_id"]+'/successful';
			}
			else {
				self.location = '/orders/order-placement/'+result["order_id"]+'/failed';
			}
		}
	});
});

$(document).on('click', '#modal-feedback', function(event){
	event.preventDefault();
	$('[data-remodal-id=div-feedback]').remodal({
		closeOnOutsideClick: false
	}).open();
});