function fnSendMessage() {
	event.preventDefault();
	$.ajax({
		url: '/messages/send',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method: 'post',
		data: {
			'token':$('#token-chat').val(),
			'txt-chat-sender':$('#txt-chat-sender').val(),
			'txt-chat-content':$('#txt-chat-content').val()
		},
		dataType: 'json',
		success: function (result) {
			if (result.sent == true)	{
				$('#txt-chat-content').val('');
				fnRefreshMessages();

				toastr.success('Message sent!');
			}
			else {
				toastr.error('Your message was not sent.');
			}
			// console.log(result.sent);
		}
	});
}
$('#frm-chat').submit(function(event){
	fnSendMessage();
});