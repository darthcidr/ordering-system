//get server time
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    $('#server-time').html('<i class = "wait icon"></i> '+(h%12) + ":" + m + ":" + s + ' ' +ampm(h));
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {
    	i = "0" + i;
    }  // add zero in front of numbers < 10
    return i;
}
function ampm(h) {
	if (h < 12) {
		return 'AM';
	}
	else {
		return 'PM';
	}
}
startTime();

var tblListOfUnclaimedOrders = $('#tblListOfUnclaimedOrders').DataTable({
    processing: true,
    ajax:{
        url: '/tables/orders/unclaimed',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'fullname', render: function(data, row, fullname, meta){
            return fullname.fname + ' ' + fullname.lname;
        }},
        {'data':'address'},
        {'data':'claimed'},
        {'data':'notes'},
        {'data':'grand_total'},
        {'data':'created_at'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button id="btnViewOrder" class="ui mini icon button"><i class = "eye icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfUnclaimedOrders tbody').on('click', '#btnViewOrder', function(event){
    var dataSelect = tblListOfUnclaimedOrders.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/orders/view/'+id);
});
var tblListOfAllOrders = $('#tblListOfAllOrders').DataTable({
    processing: true,
    ajax:{
        url: '/tables/orders/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'fullname', render: function(data, row, fullname, meta){
            return fullname.fname + ' ' + fullname.lname;
        }},
        {'data':'address'},
        {'data':'claimed'},
        {'data':'notes'},
        {'data':'grand_total'},
        {'data':'created_at'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button id="btnViewOrder" class="ui mini icon button"><i class = "eye icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfAllOrders tbody').on('click', '#btnViewOrder', function(event){
    var dataSelect = tblListOfAllOrders.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/orders/view/'+id);
});
//get count of all orders
function fnGetOrdersCount() {
    $.ajax({
        url: '/orders/unclaimed/count',
        method: 'get',
        cache: false,
        dataType: 'html',
        success: function(data){
            if (data == 0) {
                $('#unclaimed-orders-count').toggleClass('hidden',true);
            }
            else if (data != undefined) {
                $('#unclaimed-orders-count').toggleClass('hidden',false);
                $('#unclaimed-orders-count').html(data);
            }
            else {
                toastr.error('Cannot retrieve unclaimed orders count');
            }
            // console.log(data);
        }
    });

    $.ajax({
        url: '/orders/all/count',
        method: 'get',
        cache: false,
        dataType: 'html',
        success: function(data){
            if (data == 0) {
                $('#all-orders-count').toggleClass('hidden',true);
            }
            else if (data != undefined) {
                $('#all-orders-count').toggleClass('hidden',false);
                $('#all-orders-count').html(data);
            }
            else {
                toastr.error('Cannot retrieve all orders count');
            }
            // console.log(data);
        }
    });
}
fnGetOrdersCount();
function fnRefreshOrders(){
    tblListOfUnclaimedOrders.ajax.reload();
    tblListOfAllOrders.ajax.reload();
    fnGetOrdersCount();
    console.log('Refreshed');
}

var tblListOfFeedbacks = $('#tblListOfFeedbacks').DataTable({
    processing: true,
    ajax:{
        url: '/tables/feedbacks',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'email'},
        {'data':'suggestion'},
        {'data':'rating'}
    ],
    pageLength: 5,
    deferRender: true
});

var tblListOfMessages = $('#tblListOfMessages').DataTable({
    // processing: true,
    ajax:{
        url: '/lists/messages',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'sender'},
        {'data':'content'},
        {'data':'created_at'},
        {'data':null}
    ],
    columnDefs: [
        {
            targets: -1,
            data: null,
            defaultContent: '\
            <button id="btnDeleteMessage" class="ui mini icon button"><i class = "trash icon"></i></button>\
            ',
            orderable: false
        },
        {
            searchable: true,
            orderable: true,
            targets: 0
        }
    ],
    pageLength: 5,
    deferRender: true
});
//get count of messages
function fnGetMessagesCount() {
    $.ajax({
        url: '/messages/count',
        method: 'get',
        cache: false,
        dataType: 'html',
        success: function(data){
            if (data == 0) {
                $('#messages-count').toggleClass('hidden',true);
            }
            else if (data != undefined) {
                $('#messages-count').toggleClass('hidden',false);
                $('#messages-count').html(data);
            }
            else {
                toastr.error('Cannot retrieve unclaimed orders count');
            }
            // console.log(data);
        }
    });
}
fnGetMessagesCount();
function fnRefreshMessages() {
    fnGetMessagesCount();
    tblListOfMessages.ajax.reload();
}