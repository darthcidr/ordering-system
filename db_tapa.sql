/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100125
 Source Host           : localhost:3306
 Source Schema         : db_tapa

 Target Server Type    : MySQL
 Target Server Version : 100125
 File Encoding         : 65001

 Date: 12/01/2018 17:02:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for feedbacks
-- ----------------------------
DROP TABLE IF EXISTS `feedbacks`;
CREATE TABLE `feedbacks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `suggestion` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meals
-- ----------------------------
DROP TABLE IF EXISTS `meals`;
CREATE TABLE `meals`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `desc` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(65, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meals
-- ----------------------------
INSERT INTO `meals` VALUES (1, 'Tapsilog', 45.00, '2018-01-11 12:53:58', '2018-01-11 12:53:58');
INSERT INTO `meals` VALUES (2, 'Hotsilog', 30.00, '2018-01-11 12:54:03', '2018-01-11 12:54:03');
INSERT INTO `meals` VALUES (3, 'Footsilog', 30.00, '2018-01-11 12:54:07', '2018-01-11 12:54:07');
INSERT INTO `meals` VALUES (4, 'Silog', 20.00, '2018-01-11 18:23:15', '2018-01-11 18:23:51');

-- ----------------------------
-- Table structure for meals_ordered
-- ----------------------------
DROP TABLE IF EXISTS `meals_ordered`;
CREATE TABLE `meals_ordered`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `meal_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meals_ordered
-- ----------------------------
INSERT INTO `meals_ordered` VALUES (1, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (2, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (3, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (4, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (5, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (6, 1, 1, 1, '2018-01-11 12:59:24', '2018-01-11 12:59:24');
INSERT INTO `meals_ordered` VALUES (7, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (8, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (9, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (10, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (11, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (12, 1, 1, 1, '2018-01-11 12:59:25', '2018-01-11 12:59:25');
INSERT INTO `meals_ordered` VALUES (13, 2, 1, 1, '2018-01-11 23:13:43', '2018-01-11 23:13:43');
INSERT INTO `meals_ordered` VALUES (14, 3, 1, 1, '2018-01-11 23:27:06', '2018-01-11 23:27:06');
INSERT INTO `meals_ordered` VALUES (15, 4, 1, 1, '2018-01-11 23:35:01', '2018-01-11 23:35:01');
INSERT INTO `meals_ordered` VALUES (16, 5, 1, 1, '2018-01-11 23:40:33', '2018-01-11 23:40:33');
INSERT INTO `meals_ordered` VALUES (17, 6, 1, 1, '2018-01-11 23:42:45', '2018-01-11 23:42:45');
INSERT INTO `meals_ordered` VALUES (18, 7, 3, 3, '2018-01-11 23:46:11', '2018-01-11 23:46:11');
INSERT INTO `meals_ordered` VALUES (19, 8, 1, 1, '2018-01-11 23:48:49', '2018-01-11 23:48:49');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (1, 'asd', 'asd', '2018-01-11 17:47:05', '2018-01-11 17:47:05');
INSERT INTO `messages` VALUES (2, 'hahahaha', 'lance', '2018-01-11 17:48:37', '2018-01-11 17:48:37');
INSERT INTO `messages` VALUES (3, 'panget si lance', 'lance', '2018-01-11 17:48:51', '2018-01-11 17:48:51');
INSERT INTO `messages` VALUES (4, 'Hello there.', 'jaac', '2018-01-11 23:06:20', '2018-01-11 23:06:20');
INSERT INTO `messages` VALUES (5, 'Hello there..', 'obiwan_kenobi', '2018-01-11 23:07:24', '2018-01-11 23:07:24');
INSERT INTO `messages` VALUES (6, 'GENERAL KENOBI!', 'darthcaedus78@gmail.com', '2018-01-11 23:07:54', '2018-01-11 23:07:54');
INSERT INTO `messages` VALUES (7, 'You\'re a bold one.', 'darthcaedus78@gmail.com', '2018-01-11 23:08:16', '2018-01-11 23:08:16');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_11_30_121038_create_meals_ordered_table', 1);
INSERT INTO `migrations` VALUES (4, '2017_12_04_132904_create_feedbacks_table', 1);
INSERT INTO `migrations` VALUES (5, '2017_12_11_230622_create_meals_table', 1);
INSERT INTO `migrations` VALUES (6, '2017_12_24_143309_create_messages_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_01_10_232535_create_orders_table', 1);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `claimed` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `grand_total` decimal(65, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 'asd', 'asd', 'asd', 'asd', 'YES', 540.00, '2018-01-11 12:59:24', '2018-01-12 00:13:36');
INSERT INTO `orders` VALUES (2, 'Obi Wan', 'Kenobi', 'High ground.', 'I like my tapa a little toasted.', 'YES', 45.00, '2018-01-11 23:13:43', '2018-01-11 23:16:56');
INSERT INTO `orders` VALUES (3, 'Count', 'Dooku', 'Geonosis', NULL, 'YES', 45.00, '2018-01-11 23:27:06', '2018-01-11 23:28:47');
INSERT INTO `orders` VALUES (4, 'asd', 'asd', 'asd', 'asd', 'YES', 45.00, '2018-01-11 23:35:01', '2018-01-12 00:16:01');
INSERT INTO `orders` VALUES (5, 'zxc', 'zxc', 'zxc', 'zxc', 'YES', 45.00, '2018-01-11 23:40:33', '2018-01-12 00:16:08');
INSERT INTO `orders` VALUES (6, 'qwe', 'qwe', 'qwe', 'qwe', 'YES', 45.00, '2018-01-11 23:42:45', '2018-01-12 00:16:18');
INSERT INTO `orders` VALUES (7, 'Jericho', 'Castillo', '181 Electrical Road, Brgy 191 Zone 20, Pasay City', 'Palagyan ng sili', 'YES', 90.00, '2018-01-11 23:46:10', '2018-01-12 00:16:37');
INSERT INTO `orders` VALUES (8, 'Bongbong', 'Castillo', 'Diyan lang kina Kagawad Vero', 'Palagyan ng sili', 'YES', 45.00, '2018-01-11 23:48:49', '2018-01-12 00:16:26');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Jacen', 'Solo', 'darthcaedus78@gmail.com', '$2y$10$YsGDqY9tstO9meEjYXxSguGMKpuTjcxiuCmjUvvORVaw6a8YOboia', 'AXawSw7zMQHqBjXMJdjgOtAQs7SdZ9dYErWQu1xI5BntvCqUXgDcDmNQwPiI', '2018-01-11 12:53:43', '2018-01-11 12:53:43');

SET FOREIGN_KEY_CHECKS = 1;
