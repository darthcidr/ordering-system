<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'txt-order-fname',
        'txt-order-lname',
        'txt-order-address',
        'txt-order-notes',
        'nud-grand-total',
    ];
}
