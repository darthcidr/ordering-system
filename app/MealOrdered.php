<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealOrdered extends Model
{
	public $table = "meals_ordered";

    protected $fillable = [
    	'toorder'
    ];
}
