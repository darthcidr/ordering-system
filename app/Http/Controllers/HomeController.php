<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Feedback;
use App\Order;
use App\Meal;

use Charts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $averageRating = Feedback::avg('rating');

        //orders count
        $orders = Order::all();

        //meals count
        $meals = Meal::all()->count();

        $sales = DB::SELECT("
            SELECT
                CASE MONTH(created_at)
                    WHEN    1   THEN 'January'
                    WHEN    2   THEN 'February'
                    WHEN    3   THEN 'March'
                    WHEN    4   THEN 'April'
                    WHEN    5   THEN 'May'
                    WHEN    6   THEN 'June'
                    WHEN    7   THEN 'July'
                    WHEN    8   THEN 'August'
                    WHEN    9   THEN 'September'
                    WHEN    10  THEN 'October'
                    WHEN    11  THEN 'November'
                    WHEN    12  THEN 'December'

                END AS 'Month',
                SUM(grand_total)
            FROM
                orders
            WHERE
                YEAR(created_at) = ".date("Y")."
            GROUP BY
                MONTH(created_at)
        ");

        $totalSales = Order::where('claimed','YES')->whereYear('created_at',date('Y'))->sum('grand_total');

        $chart = Charts::database(Order::where('claimed','YES')->get(), 'line', 'highcharts')
                    ->title('Sales Report')
                    ->responsive(true)
                    ->xAxisTitle('Month')
                    ->yAxisTitle('Sales (PHP)')
                    ->elementLabel('Total '.date('Y').' sales = '.$totalSales.' PHP')
                    ->aggregateColumn('grand_total','sum')
                    ->groupByMonth()
                    ->labels(['January','February','March','April','May','June','July','August','September','October','November','December']);


        return view('admin.home')->with([
            'average'   =>  $averageRating,
            'orders'    =>  $orders,
            'meals'     =>  $meals,
            'chart'     =>  $chart
        ]);
        // return view('admin.home');
        // return $sales;
    }
}
