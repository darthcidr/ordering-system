<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Meal;
use App\Order;
use App\Feedback;

class TablesController extends Controller
{
    public function meals()
    {
    	$meals = Meal::all();

        return $meals;
    }

    public function all_orders()
    {
        $all_orders = Order::orderBy('updated_at','desc')->get();

        return $all_orders;
    }

    public function unclaimed_orders()
    {
    	$unclaimed_orders = Order::where('claimed','NO')->orderBy('updated_at','desc')->get();

    	return $unclaimed_orders;
    }

    public function feedbacks()
    {
        $feedbacks = Feedback::all();

        return $feedbacks;
    }
}
