<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ThanksForYourFeedback;

use App\Feedback;
use DB;

class FeedbacksController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        $this->middleware('auth')->except([
            'store'
        ]);
    }

    public function index()
    {
        return view('admin.feedbacks.index');
    }
    
    public function store(Request $request)
    {
        $feedback = new Feedback;
        $feedback->email        = $request['feedback-email'];
        $feedback->rating       = $request['feedback-satisfaction-rating'];
        $feedback->suggestion   = $request['feedback-suggestions'];
        $feedback->save();

        $user = Feedback::where('email',$request['feedback-email'])->first();
        $user->notify(new ThanksForYourFeedback());

        return redirect('/')->with('status','Thanks for your feedback!');
    }
}
