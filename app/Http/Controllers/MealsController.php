<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;

class MealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $meals = Meal::all();

        // return view('admin.meals.meals')->with('meals',$meals);
        return view('admin.meals.index');
    }

    public function get()
    {
        $meals = Meal::all();

        return $meals;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'desc'  =>  'required',
        //     'price' =>  'required'
        // ]);

        $meal = new Meal;
        $meal->desc = $request["meal-desc"];
        $meal->price = $request["meal-price"];
        $meal->save();

        return redirect('/meals')->with('success','Meal added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meal = Meal::find($id);

        return view('admin.meals.view')->with('meal',$meal);
        // return $meal;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'desc'  =>  'required',
        //     'price' =>  'required'
        // ]);

        $meal = Meal::find($id);
        $meal->desc = $request->input("edit-meal-desc");
        $meal->price = $request->input("edit-meal-price");
        $meal->save();

        return redirect('/meals')->with('success','Meal edited successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meal = Meal::find($id);
        $meal->delete();

        return redirect('/meals')->with('success','Meal deleted successfully.');
    }
}
