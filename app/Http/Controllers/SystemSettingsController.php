<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SystemSettingsController extends Controller
{
    public function index()
    {
    	return view('admin.settings.index');
    }

    public function database_backup(Request $request)
    {
		// $dbBackupFilenameDefault = env('DB_DATABASE')."-backup_".date('mdY-Hi');
		// $dbBackupFileName = "";

		// if (($request["txtBackupDBFileName"] === null) || ($request["txtBackupDBFileName"] == '')) {
		// 	$dbBackupFileName = $dbBackupFilenameDefault;
		// }
		// else {
		// 	$dbBackupFileName = $request["txtBackupDBFileName"];
		// }

		// $strBackupDB = ("mysqldump ".env('DB_DATABASE')." -u ".env('DB_USERNAME')." > ".$request["txtBackupDBFileDir"]."/".$dbBackupFileName.".sql");

		// $strBackupResult = exec($strBackupDB,$output,$return_var);
		// if (($output === null)) {
		// 	return redirect('/settings')->with([
		// 		'message'	=>	'Database backup failed.',
		// 		'status'	=>	'error'
		// 	]);
		// }
		// else {
		// 	return redirect('/settings')->with([
		// 		'message'	=>	'Database backup successful.',
		// 		'status'	=>	'success'
		// 	]);
		// }
		return 'THIS FEATURE IS IN MAINTENANCE MODE. SORRY FOR THE INCONVINIENCE.';
    }

    public function database_restore(Request $request)
    {

    }
}
