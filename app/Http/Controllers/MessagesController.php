<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function index()
    {
        return view('admin.messages.index');
    }

    public function send(Request $request)
    {
    	$sent = false;
    	$message = new Message;
    	$message->content = $request["txt-chat-content"];
    	$message->sender = $request["txt-chat-sender"];
    	$sent = $message->save();

    	return response()->json([
            "sent"	=>  $sent
        ]);
    }

    public function get()
    {
    	$messages = Message::orderBy('created_at','DESC')->get();
    	// $messages = Message::all();

    	return $messages;
    }

    public function count()
    {
        $messages = Message::count();

        return $messages;
    }
}
