<?php

namespace App\Http\Controllers;
use App\Order;

use Illuminate\Http\Request;
use DB;

class ReportsController extends Controller
{
    public function orders() {
    	$orders = Order::all();

    	return view('admin.reports.orders')->with('orders',$orders);
    }

    public function sales(Request $request) {
        $sales = Order::whereYear('created_at',$request['cmbSalesYear'])->get();

        $total_sales = Order::whereYear('created_at',$request['cmbSalesYear'])->sum('grand_total');

        //convert computed total sales
        // $total_sales_in_words = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        return view('admin.reports.sales')->with([
            'year'                  =>  $request['cmbSalesYear'],
            'sales'                 =>  $sales,
            'total_sales'           =>  $total_sales,
            // 'total_sales_in_words'  =>  $total_sales_in_words->format($total_sales)
        ]);
        
    }

    public function customers()
    {
    	$customers = Order::select([
            'id',
            'fname',
            'lname',
            'address',
            'created_at',
        ])->get();

    	return view('admin.reports.customers')->with('customers',$customers);
    }

    public function receipt($id)
    {
        $order = Order::find($id);

        // $meals_ordered = MealOrdered::where('order_id',$id)->get();
        $meals_ordered = DB::SELECT("
            SELECT
                meals_ordered.id,
                meals_ordered.order_id,

                meals_ordered.meal_id,
                meals.desc,
                meals.price,

                meals_ordered.qty,
                (meals.price*meals_ordered.qty) AS 'total',

                meals_ordered.created_at,
                meals_ordered.updated_at
            FROM
                meals_ordered
                    INNER JOIN meals ON meals_ordered.meal_id = meals.id
            WHERE
                meals_ordered.order_id = '".$id."';
        ");

        return view('admin.orders.receipt')->with([
            'order' =>  $order,
            'meals_ordered' =>  $meals_ordered
        ]);
        // return $meals_ordered;
    }
}
