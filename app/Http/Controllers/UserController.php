<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.account-settings');
    }

    public function update(Request $request, $id)
    {
        // $image = $request->file('picture');
        // $fileName = $image->getClientOriginalName();
        // $request['picture'] = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path('/images');
        // $image->move($destinationPath, $request['picture']);
        $imageName = time().'.'.request()->picture->getClientOriginalExtension();
        request()->picture->move(public_path('/images/profile_picture'), $imageName);

        $user = User::find($id);
        $user->fname = $request['fname'];
        $user->lname = $request['lname'];
        $user->image = $imageName;
        $user->email = $request['email'];
        $user->update();

        return redirect('/account-settings')->with([
            'status'    =>  'Account updated successfully.'
        ]);
    }
}
