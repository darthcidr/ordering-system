<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;

class DropdownsController extends Controller
{
    public function meals()
    {
    	$meals = Meal::all();

    	// return view('index')->with('meals',$meals);
    	return $meals;
    }
}
