<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Meal;
use App\Order;
use App\MealOrdered;

use Nexmo; //send sms using nexmo

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_unclaimed()
    {
        return view('admin.orders.unclaimed');
    }
    public function index_all()
    {
        return view('admin.orders.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $str_meals_ordered = "";

        $saved = false;

        $order = new Order;
        $order->fname = $request["txt-order-fname"];
        $order->lname = $request["txt-order-lname"];
        $order->contact_number = $request["txt-order-contact-number"];
        $order->address = $request["txt-order-address"];
        $order->notes = $request["txt-order-notes"];
        $order->claimed = "NO";
        $order->grand_total = $request["nud-grand-total"];
        $saved = $order->save();
        $last_inserted = $order->id;

        if ($saved == true) {
            for ($i=0 ; $i<count($request["toorder"]) ; $i++) {
                $meal_ordered = new MealOrdered;
                $meal_ordered->order_id        = $last_inserted;
                $meal_ordered->meal_id         = $request["toorder"][$i]["mealid"];
                $meal_ordered->qty             = $request["toorder"][$i]["mealqty"];
                $saved = $meal_ordered->save();
                $str_meals_ordered = $str_meals_ordered . $request["toorder"][$i]["mealdesc"] . ' x ' . $request["toorder"][$i]["mealqty"] . ', ';
            }

            if ($saved == true) {
                $notification = Nexmo::message()->send([
                    'to' => '+63'.$request["txt-order-contact-number"],
                    'from' => '@leggetter',
                    'text' => 'Good day, Mr/Ms '.strtoupper($request["txt-order-fname"]).' '.strtoupper($request["txt-order-lname"]).'. This is to notify you about your order/s under #'.$last_inserted.': '.trim($str_meals_ordered,', ').'. Thank you for your patronage!'
                ]);
                // \Log::info('sent message: ' . $notification['message-id']);

                return response()->json([
                    "saved"     =>  $saved,
                    "order_id"  =>  $last_inserted
                ]);
            }
            else {
                return response()->json([
                    "saved"     =>  $saved,
                    "order_id"  =>  $last_inserted
                ]);
            }
        }
        else {
            return response()->json([
                "saved"     =>  $saved,
                "order_id"  =>  $last_inserted
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        // $meals_ordered = MealOrdered::where('order_id',$id)->get();
        $meals_ordered = DB::SELECT("
            SELECT
                meals_ordered.id,
                meals_ordered.order_id,

                meals_ordered.meal_id,
                meals.desc,
                meals.price,

                meals_ordered.qty,
                (meals.price*meals_ordered.qty) AS 'total',

                meals_ordered.created_at,
                meals_ordered.updated_at
            FROM
                meals_ordered
                    INNER JOIN meals ON meals_ordered.meal_id = meals.id
            WHERE
                meals_ordered.order_id = '".$id."';
        ");

        return view('admin.orders.view')->with([
            'order' =>  $order,
            'meals_ordered' =>  $meals_ordered
        ]);
        // return $meals_ordered;required 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->claimed = "YES";
        $order->update();

        return redirect('/orders')->with('success','Order claimed.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unclaimed()
    {
        $unclaimed = Order::where('claimed','NO')->count();

        return $unclaimed;
    }

    public function all()
    {
        $all = Order::all()->count();

        return $all;
    }

    public function search_form()
    {
        return view('admin.orders.search');
    }

    public function search(Request $request)
    {
        // $orders = Order::where('id', 'LIKE', '%'.$request['txt-search-order-number'].'%')->get();
        $orders = DB::SELECT("
            SELECT
                *
            FROM
                orders
            WHERE
                id      LIKE '%".$request['txt-search-order-number']."%'
                OR
                fname   LIKE '%".$request['txt-search-order-number']."%'
                OR
                lname   LIKE '%".$request['txt-search-order-number']."%'
        ");

        return $orders;
    }
}
