<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use Notifiable;
	
	public $table = "feedbacks";

    protected $fillable = [
    	'feedback-satisfaction-rating',
    	'feedback-email'
    ];
}
