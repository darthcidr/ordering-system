<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');
// User Settings Route
Route::get('/account-settings', 'UserController@index')->middleware('auth');
Route::put('/account-settings/update/{user}', [
	'as'	=>	'account.update',
	'uses' 	=> 	'UserController@update'
])->middleware('auth');
// Logout Route
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

//tables
Route::get('/tables/meals', 'TablesController@meals')->middleware('auth');
Route::get('/tables/orders/unclaimed', 'TablesController@unclaimed_orders')->middleware('auth');
Route::get('/tables/orders/all', 'TablesController@all_orders')->middleware('auth');
Route::get('/tables/feedbacks', 'TablesController@feedbacks')->middleware('auth');

//dropdowns
Route::get('/lists/meals', 'DropdownsController@meals');

//orders
Route::get('/orders/unclaimed', 'OrdersController@index_unclaimed')->middleware('auth');
Route::get('/orders/all', 'OrdersController@index_all')->middleware('auth');
Route::post('/orders/order-placement/new', 'OrdersController@store');
Route::get('/orders/order-placement/{order}/successful', function($order){
	return redirect('/')->with([
		'status'	=>	'Order #'.sprintf('%07d',$order).' placement successful.',
		// 'info'		=>	'Your order has been placed under Order #'.sprintf('%07d',$order)
	]);
});
Route::get('/orders/order-placement/{order}/failed', function($order){
	return redirect('/')->with([
		'status'	=>	'Order #'.sprintf('%07d',$order).' placement failed.',
		// 'info'		=>	'Your order has been placed under Order #'.sprintf('%07d',$order)
	]);
});
Route::get('/orders', 'OrdersController@index')->middleware('auth');
Route::get('/orders/view/{order}', 'OrdersController@show');
Route::put('/orders/claim/{order}', 'OrdersController@update')->name('orders.claim')->middleware('auth');
Route::get('/orders/unclaimed/count', 'OrdersController@unclaimed')->middleware('auth');
Route::get('/orders/all/count', 'OrdersController@all')->middleware('auth');
Route::get('/orders/search-form', 'OrdersController@search_form');
Route::post('/orders/search', 'OrdersController@search');

//feedbacks
Route::resource('/feedbacks', 'FeedbacksController');

//newsletter
Route::post('/ads/send/email', 'AdvertisementsController@email')->middleware('auth')->name('ad.sendmail');

//meals
Route::resource('/meals', 'MealsController');

//chat
Route::get('/chat', function(){
	return view('chat');
});
Route::post('/messages/send', 'MessagesController@send');
Route::get('/lists/messages', 'MessagesController@get');
Route::get('/messages', 'MessagesController@index')->middleware('auth');
Route::get('/messages/count', 'MessagesController@count')->middleware('auth');

//system settings
Route::get('/settings', 'SystemSettingsController@index')->middleware('auth');
Route::post('/settings/database/backup', 'SystemSettingsController@database_backup')->middleware('auth');

//generate reports
Route::get('/reports/list-of-orders', 'ReportsController@orders')->middleware('auth');
Route::get('/reports/list-of-customers', 'ReportsController@customers')->middleware('auth');
Route::post('/reports/sales', 'ReportsController@sales')->middleware('auth');
Route::get('/reports/receipt/order/{order}', 'ReportsController@receipt');
