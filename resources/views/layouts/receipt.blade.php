<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">

        <style>
            footer {
                position: fixed;
                bottom: 0px;
                right: 0px;

                font-size: 6px;
            }
        </style>
    </head>

    <body>
        <div class = "ui center aligned container">
            <h5 class = "ui header">
                <div class = "content">
                    TAPA NI MAU
                    <div class = "sub header">Electrical Road, Brgy. 191 Zone 20</div>
                    <div class = "sub header">Pasay City, Philippines</div>
                </div>
            </h5>
            <h5 class = "ui header">@yield('report-title')</h5>
        </div>

        <div class = "ui fluid container" style = "padding-top: 1rem; font-size: 12px;">
            <div class = "ui stackable equal width grid">
                @yield('content')
            </div>
        </div>

        {{-- <footer>
            @yield('page-name') / Printed by: {{ Auth::user()->fname }} {{ Auth::user()->lname }} / {{ date('Y-m-d h:m:s') }}
        </footer> --}}

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
        <script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
        <script src="{{ asset('js/core.js') }}"></script>
        <script type="text/javascript">
            window.onload = function(){
                window.print();
            }
        </script>

        @yield('scripts')
    </body>
</html>