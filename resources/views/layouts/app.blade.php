<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
        @if(Auth::check())
            @yield('charts-css')
        @endif
    </head>

    <body>
        @include('includes.navbar-top')
        @if(Auth::check())
            @include('includes.navbar-side')

            <div class = "ui fluid container" style = "padding-left: 17rem; padding-right: 2rem; padding-top: 5rem;">
                <div class = "ui stackable equal width grid">
                    @yield('content')
                </div>
            </div>
        @else
            <div class = "ui fluid container" style = "padding-top: 5rem;">
                <div class = "ui stackable equal width grid">
                    @yield('content')
                </div>
            </div>
        @endif

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
        <script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/dataTables/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('packages/dataTables/dataTables.semanticui.js') }}"></script>
        <script src="{{ asset('packages/Remodal/dist/remodal.js') }}"></script>
        <script src="{{ asset('packages/jquery-mask/dist/jquery.mask.js') }}"></script>
        <script src="{{ asset('packages/nanobar/nanobar.js') }}"></script>
        <script src="{{ asset('js/core.js') }}"></script>
        @if(Auth::check())
            <script src="{{ asset('js/admin/functions.js') }}"></script>
            @yield('charts-js')
        @endif
        @yield('scripts')
    </body>
</html>