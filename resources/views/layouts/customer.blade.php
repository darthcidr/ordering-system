<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
    </head>

    <body>
        @include('includes.navbar-top-customer')

        <div class = "ui container" style = "padding-top: 6rem;">
            <div class = "ui stackable equal width grid">
                @yield('content')
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
        <script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
        <script src="{{ asset('js/core.js') }}"></script>

        @yield('scripts')
    </body>
</html>