@section('page-name')
    {{ config('app.name', 'Laravel') }} | Chat
@endsection

@extends('layouts.customer')

@section('content')
    <div class = "row">
    	<div class = "column"></div>

    	<div class = "ten wide column">
			<div class = "ui small raised clearing text segment">
				<h4 class = "ui header">
					<i class = "comments icon"></i>
					Chat
				</h4>

				<form id = "frm-chat" class = "ui small equal width form">
					<input type="hidden" id="token-chat" value="{{ csrf_token() }}">

					<div class = "field">
						<label>Username:</label>
						@if(Auth::check())	
							<input type = "text" id = "txt-chat-sender" name = "txt-chat-sender" value = "{{ Auth::user()->email }}" readonly required>
						@else
							<input type = "text" id = "txt-chat-sender" name = "txt-chat-sender" required>
						@endif
					</div>

					<div id = "div-chat-messages" class = "ui segment field" style = "height: 17rem; overflow: auto;">

					</div>

					<div class = "field">
						<label>Message:</label>
						<textarea id = "txt-chat-content" name = "txt-chat-content" rows = "3" placeholder = "Your message here..." required></textarea> 
					</div>

					<button id = "btn-chat-send" name = "btn-chat-send" class = "ui small positive button">
						<i class = "send icon"></i>
						Send
					</button>
				</form>
			</div>
		</div>

		<div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.success('{{ session('status') }}');
        </script>
    @endif

    <script src="{{ asset('js/chat.js') }}"></script>
@endsection