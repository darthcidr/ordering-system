@section('page-name')
    {{ config('app.name', 'Laravel') }} | Register
@endsection

@extends('layouts.app')

@section('content')
    <div class = "row">
        <div class = "column"></div>
        <div class = "nine wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Register</h5>

                <form id = "frmRegister" class = "ui small equal width form" method = "POST" action = "{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class = "fields">
                        <div class="field">
                            <label>Password:</label>
                            <input id="password" type="password" name="password" required>
                        </div>

                        <div class="field">
                            <label>Confirm password:</label>
                            <input id="password-confirm" type="password" name="password_confirmation" required>
                        </div>
                    </div>

                    <button type = "submit" class = "ui small positive button">Register</button>
                    <button type = "reset" class = "ui small negative button">Reset fields</button>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgRegister').toggleClass('active',true);

        $('#password, #password-confirm').on('keyup click', function(){
            if ($('#password').val() != $('#password-confirm').val()) {
                $('#password, #password-confirm').css({
                    'border-color':'red'
                });
            }
            else {
                $('#password, #password-confirm').css({
                    'border-color':''
                });
            }
        });
    </script>
@endsection