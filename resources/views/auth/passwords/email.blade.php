@section('page-name')
    {{ config('app.name', 'Laravel') }} | Reset password
@endsection

@extends('layouts.app')

@section('content')
    <div class = "row">
        <div class = "column"></div>
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Reset password</h5>

                <form class="ui small equal width form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="field{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="field">
                            <label>E-Mail Address</label>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                        </div>

                        <button type="submit" class="ui small primary button">Send password reset link</button>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if (session('status'))
        <script type = "text/javascript">
            toastr.info('{{ session('status') }}');
        </script>
    @endif

    @if ($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}');
        </script>
    @endif
@endsection
