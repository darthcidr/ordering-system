@section('page-name')
    {{ config('app.name', 'Laravel') }} | System Settings
@endsection

@extends('layouts.app')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui fluid negative icon message">
                <i class = "warning icon"></i>
                These features are not yet stable. Sorry for the inconvenience.
            </div>
            
            <div class = "ui small raised text segment">
                <h4 class = "ui header">Database settings</h4>

                <div class = "ui two basic horizontal segments">
                    <div class = "ui segment">
                        <h5 class = "ui header">Backup</h5>

                        <form id = "frmDB" class = "ui small equal width form" action = "/settings/database/backup" method = "post">
                            {{ csrf_field() }}

                            <div class = "field">
                                <label>Directory</label>
                                <input type = "text" id = "txtBackupDBFileDir" name = "txtBackupDBFileDir" value = "C:" required>
                            </div>

                            <div class = "field">
                                <label>File name</label>
                                <input type = "text" id = "txtBackupDBFileName" name = "txtBackupDBFileName">
                            </div>

                            <button type = "submit" class = "ui small positive button">
                                <i class = "download icon"></i>
                                Backup database
                            </button>
                        </form>
                    </div>

                    <div class = "ui segment">
                        <h5 class = "ui header">Restore</h5>

                        <form id = "frmDB" class = "ui small equal width form" action = "/settings/database/restore" method = "post" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class = "field">
                                <label>Database to restore</label>
                                <input type = "file" id = "fileRestoreDB" name = "fileRestoreDB" accept = ".sql" required>
                            </div>

                            <button type = "submit" class = "ui small positive button">
                                <i class = "upload icon"></i>
                                Restore database
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status') && session()->has('message'))
        <script type = "text/javascript">
            toastr.{{ session('status') }}('{{ session('message') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettings').toggleClass('active',true);
    </script>
@endsection
