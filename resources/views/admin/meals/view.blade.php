@section('page-name')
	{{ config('app.name', 'Laravel') }} | Meals
@endsection

@extends('layouts.app')

@section('content')
	<div class = "row">
		<div class = "column"></div>
		<div class = "eight wide column">
			<div class = "ui small raised clearing text segment">
				<h5 class = "ui header">Edit meal</h5>

				<form id = "frmEditMeal" class = "ui small equal width form" method = "post" action = "{{ route('meals.update',$meal->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "field">
						<label>Description:</label>
						<input type = "text" id = "edit-meal-desc" name = "edit-meal-desc" value = "{{ $meal->desc }}" required autofocus>
					</div>

					<div class = "field">
						<label>Price:</label>
						<input type = "number" id = "edit-meal-price" name = "edit-meal-price" value = "{{ $meal->price }}" min = "0" step = "0.01" required>
					</div>

					<div class = "ui three small buttons">
						<a href = "/meal" role = "button" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>
						<button type = "submit" class = "ui small positive button">
							<i class = "send icon"></i>
							Submit
						</button>
						<button type = "button" id = "btnDeleteMeal" class = "ui small negative button">
							<i class = "trash icon"></i>
							Delete
						</button>
					</div>
				</form>

				<form id = "frmDeleteMeal" method = "post" action = "{{ route('meals.destroy',$meal->id) }}" hidden>
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				</form>
			</div>
		</div>
		<div class = "column"></div>
	</div>
@endsection

@section('scripts')
	@if(session()->has('success'))
		<script type = "text/javascript">
			toastr.success('{{ session('success') }}');
		</script>
	@endif

	<script type = "text/javascript">
		$('#pgMeals').toggleClass('active',true);

		var tblListOfMeals = $('#tblListOfMeals').DataTable();

		$(document).on('click','#btnDeleteMeal',function(){
			if (confirm('Confirm this action?') == true) {
				$('#frmDeleteMeal').submit();
			}
		});
	</script>
@endsection