@section('page-name')
	{{ config('app.name', 'Laravel') }} | Meals
@endsection

@extends('layouts.app')

@section('content')
	{{-- <div data-remodal-id="modalEditUsers">
		<button data-remodal-action="close" class="remodal-close"></button>
		<h5 class = "header">Edit user</h5>

	</div> --}}

	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small raised text segment">
				<h5 class = "ui header">Add meal</h5>

				<form id = "frmAddMeal" class = "ui small equal width form" method = "post" action = "{{ route('meals.store') }}">
					{{ csrf_field() }}

					<div class = "field">
						<label>Description:</label>
						<input type = "text" id = "meal-desc" name = "meal-desc" required autofocus>
					</div>

					<div class = "field">
						<label>Price:</label>
						<input type = "number" id = "meal-price" name = "meal-price" min = "0" step = "0.01" required>
					</div>

					<button type = "submit" class = "ui small primary button">Submit</button>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small raised text segment">
				<h5 class = "ui header">List of meals</h5>

				<table id = "tblListOfMeals" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Description</th>
							<th>Price</th>
							<th>Action</th>
						</tr>
					</thead>

					{{-- <tbody>
						@if(count($meals) > 0)
							@foreach($meals as $meal)
								<tr>
									<td>{{ $meal->id }}</td>
									<td>{{ $meal->desc }}</td>
									<td>{{ $meal->price }}</td>
									<td>
										<button type = "button" id = "btnEditMeal" class = "ui mini icon button">
											<i class = "eye icon"></i>
										</button>
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td>No data available</td>
							</tr>
						@endif
					</tbody> --}}
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	@if(session()->has('success'))
		<script type = "text/javascript">
			toastr.success('{{ session('success') }}');
		</script>
	@endif

	<script type = "text/javascript">
		$('#pgMeals').toggleClass('active',true);

		var tblListOfMeals = $('#tblListOfMeals').DataTable({
			processing: true,
			ajax:{
				url: '/tables/meals',
				dataSrc: '',
				serverside: true
			},
			columns: [
				{'data':'id'},
				{'data':'desc'},
				{'data':'price'},
				{'data':null}
			],
			columnDefs: [{
				targets: -1,
				data: null,
				defaultContent: '\
					<button id="btnViewMeal" class="ui mini icon button"><i class = "eye icon"></i></button>\
				',
				orderable: false
			},{
				searchable: true,
				orderable: true,
				targets: 0
			}],
			pageLength: 5
		});

		$('#tblListOfMeals tbody').on('click', '#btnViewMeal', function(event){
			var dataSelect = tblListOfMeals.row( $(this).parents('tr') ).data();
			var id = dataSelect['id'];
			self.location = '/meals/'+id+'/edit';
		});
	</script>
@endsection