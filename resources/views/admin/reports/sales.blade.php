{{-- @if(count($orders) > 0)
	@foreach($orders as $order)
		{{ $order->id }}
	@endforeach
@else
	No orders
@endif --}}

@section('page-name')
	{{ config('app.name', 'Laravel') }} - Reports | Sales
@endsection

@extends('layouts.report')
@section('report-title')
{{ $year }} SALES REPORT
@endsection

@section('content')
	<div class = "ui basic vertical segment">
		<table border = "1" cellspacing="0" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Customer name</th>
					<th>Address</th>
					<th>Date</th>
					<th>Grand Total (PHP)</th>
				</tr>
			</thead>

			<tbody>
				@if(count($sales) > 0)
					@foreach($sales as $sale)
						<tr>
							<td>{{ sprintf('%07d', $sale->id) }}</td>
							<td>{{ $sale->fname }} {{ $sale->lname }}</td>
							<td>{{ $sale->address }}</td>
							<td>{{ date('F d, Y',strtotime($sale->created_at)) }}</td>
							<td>{{ $sale->grand_total }}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan = "6">No orders placed yet.</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>

	<div class = "ui basic right aligned vertical segment">
		TOTAL SALES: <h3 class = "ui header">PHP {{ $total_sales }}</h3>
	</div>
	{{-- <div class = "ui basic right aligned vertical segment">
		<h3 class = "ui header">
			<div class = "content">
				<div class = "sub header">Total Sales:</div>
				PHP {{ $total_sales }} ({{ strtoupper($total_sales_in_words) }} PESOS only)
			</div>
		</h3>
	</div> --}}

@endsection

@section('signatories')
	<h4 class = "ui header">Records above are hereby certified true and accurate,</h4>
	<div class = "ui basic vertical segment" style = "margin-top: 5rem;">
		<div class = "ui fluid container">
			<div class = "ui stackable equal width center aligned grid">
				<div class = "row">
					<div class = "column">
						<h4 class = "ui header">
							<div class = "content">
								Mau
								<div class = "sub header">Chief Executive Officer</div>
							</div>
						</h4>
					</div>

					<div class = "column">
						<h4 class = "ui header">
							<div class = "content">
								John Doe
								<div class = "sub header">Internal Accountant</div>
							</div>
						</h4>
					</div>

					<div class = "column">
						<h4 class = "ui header">
							<div class = "content">
								Juan Dela Cruz
								<div class = "sub header">External Accountant</div>
							</div>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection