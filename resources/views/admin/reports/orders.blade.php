{{-- @if(count($orders) > 0)
	@foreach($orders as $order)
		{{ $order->id }}
	@endforeach
@else
	No orders
@endif --}}

@section('page-name')
	{{ config('app.name', 'Laravel') }} - Reports | List of Orders
@endsection

@extends('layouts.report')
@section('report-title')
LIST OF ORDERS
@endsection

@section('content')
	<div class = "ui basic segment">
		<table border = "1" cellspacing="0" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Customer name</th>
					<th>Address</th>
					<th>Notes</th>
					<th>Claimed</th>
					<th>Date placed</th>
				</tr>
			</thead>

			<tbody>
				@if(count($orders) > 0)
					@foreach($orders as $order)
						<tr>
							<td>{{ sprintf('%07d',$order->id) }}</td>
							<td>{{ $order->fname }} {{ $order->lname }}</td>
							<td>{{ $order->address }}</td>
							<td>{{ $order->notes }}</td>
							<td>{{ $order->claimed }}</td>
							<td>{{ date("F d, Y h:i A",strtotime($order->created_at)) }}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan = "6">No orders placed yet.</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
@endsection