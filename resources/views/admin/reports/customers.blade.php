{{-- @if(count($orders) > 0)
	@foreach($orders as $order)
		{{ $order->order_id }}
	@endforeach
@else
	No orders
@endif --}}

@section('page-name')
	{{ config('app.name', 'Laravel') }} - Reports | List of Customers
@endsection

@extends('layouts.report')
@section('report-title')
LIST OF CUSTOMERS
@endsection

@section('content')
	<div class = "ui basic segment">
		<table border = "1" cellspacing="0" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Address</th>
					<th>Date ordered</th>
				</tr>
			</thead>

			<tbody>
				@if(count($customers) > 0)
					@foreach($customers as $customer)
						<tr>
							<td>{{ $customer->id }}</td>
							<td>{{ $customer->fname }} {{ $customer->lname }}</td>
							<td>{{ $customer->address }}</td>
							<td>{{ date("F d, Y, h:i A",strtotime($customer->created_at)) }}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan = "6">No customers yet.</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
@endsection