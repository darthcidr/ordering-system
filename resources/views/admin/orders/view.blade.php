@section('page-name')
	{{ config('app.name', 'Laravel') }} | Order # {{ $order->id }}
@endsection

@extends('layouts.app')

@section('content')
	<div class = "row">
		<div class = "column"></div>
		<div class = "eight wide column">
			<div class = "ui small raised clearing text segment">
				<h5 class = "ui header">Edit meal</h5>

				<form id = "frm-claim-order" class = "ui small equal width form" method = "post" action = "{{ route('orders.claim',$order->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "field">
						<label>Meal/s ordered:</label>
						<div class = "ui relaxed list">
							@if(count($meals_ordered)>0)
								@foreach($meals_ordered as $meal_ordered)
									<div class = "item">
										<i class = "food icon"></i>
										{{ $meal_ordered->desc }} x {{ $meal_ordered->qty }} = {{ $meal_ordered->total }} PHP
									</div>
								@endforeach
							@else
								<li>No orders found</li>
							@endif
						</div>
					</div>

					<div class = "field">
						<label>Notes:</label>
						<span>{{ $order->notes }}</span>
					</div>

					<div class = "inline field">
						<label>Grand total:</label>
						<span>{{ $order->grand_total }} PHP</span>
					</div>

					<div class = "inline field">
						<label>Claimed?</label>
						<span>{{ $order->claimed }}</span>
					</div>

					<div class = "ui three small buttons">
						<a href = "javascript:;" onclick = "window.history.back()" role = "button" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>
						@if($order->claimed == 'YES')
							<button type = "submit" id = "btn-claim-order" class = "ui disabled small negative button">
								Claimed
							</button>
						@else
							<button type = "submit" id = "btn-claim-order" class = "ui small positive button">
								Claim
							</button>
						@endif
						<a href = "/reports/receipt/order/{{ $order->id }}" role = "button" class = "ui small button" target = "_new">
							<i class = "print icon"></i>
							Print
						</a>
					</div>
				</form>
			</div>
		</div>
		<div class = "column"></div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$('#pgOrders').toggleClass('active',true);

		$(document).on('click', '#btn-claim-order', function(event){
			event.preventDefault();
			if (confirm('Confirm claim order #{{ $order->id }}?') == true) {
				$('#frm-claim-order').submit();
			}
		});
	</script>
@endsection