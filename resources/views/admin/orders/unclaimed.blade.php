@section('page-name')
    {{ config('app.name', 'Laravel') }} | Orders
@endsection

@extends('layouts.app')

@section('content')
    <div class = "row">        
        <div class = "column">
            <div class = "ui small raised clearing text segment">
                <h3 class = "ui header">
                    Unclaimed orders
                </h3>

                <table id = "tblListOfUnclaimedOrders" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Address</th>
                            <th>Claimed</th>
                            <th>Notes</th>
                            <th>Grand Total</th>
                            <th>Date placed</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>

                <div class = "ui basic center aligned segment">
                    <button type = "button" id = "btn-refresh-table" class = "ui small button" onclick = "fnRefreshOrders()">
                        <i class = "refresh icon"></i>
                        Reload table
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgOrders').toggleClass('active',true);
    </script>
@endsection