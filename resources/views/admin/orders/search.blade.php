@section('page-name')
	{{ config('app.name', 'Laravel') }} | Search
@endsection

@extends('layouts.customer')

@section('content')
	<div class = "row">
		<div class = "column"></div>

		<div class = "eight wide column">
			<div class = "ui small text segment">
				<h4 class = "ui header">Search order</h4>

				<div id = "frm-search-order" class = "ui small equal width form">
					<input type="hidden" id="token" value="{{ csrf_token() }}">

					<div id = "search-form" class="ui fluid icon input">
						<input class="prompt" type="text" id = "txt-search-order-number" placeholder="Enter your order number here" autofocus>
						<i class="search icon"></i>
					</div>
				</div>
			</div>

			<div class = "ui basic segment">
				<h4 class = "ui header">Search results: <span id = "results-found"></span></h4>
				<div id = "search-results" class = "ui relaxed ordered list">

				</div>
			</div>
		</div>

		<div class = "column"></div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$('#pgOrders').toggleClass('active',true);

		//search mecahnism
		$(document).on('keyup', '#txt-search-order-number', function(event){
			$('#search-form').toggleClass('loading',true);
			setTimeout(function(){
				$('#search-form').toggleClass('loading',false);
				event.preventDefault();
				$.ajax({
					url: '/orders/search',
					method: 'post',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {
						'token'						: $('#token').val(),
						'txt-search-order-number'	: $('#txt-search-order-number').val()
					},
					dataType: 'json',
					beforeSend: function(xhr){
						if ($('#txt-search-order-number').val() == '') {
							xhr.abort();
							$('#search-results,#results-found').empty();
							// toastr.warning('Please enter your order number in the text box provided.');
						}
					},
					success: function(data){
						if (data != undefined) {
							$('#search-results,#results-found').empty();
							$('#results-found').html('('+data.length+' found)');
							$.each(data, function(i,val){
								$('#search-results').append(
									'<a href = "/reports/receipt/order/'+val.id+'" class = "item" target = "_new">'+
										'#'+val.id+', '+val.fname+' '+val.lname+', '+val.created_at+
									'</a>'
								);
							});
						}
						else {
							toastr.error('Cannot retrieve results.');
						}
						console.log(data);
					}
				});
			},1000);
		});
	</script>
@endsection