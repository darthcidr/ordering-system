@section('page-name')
	{{ config('app.name', 'Laravel') }} - Reports | Receipt
@endsection

@extends('layouts.receipt')
@section('report-title')
OFFICIAL RECEIPT #{{ sprintf('%07d', $order->id) }}
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui basic text segment form">
				<div class = "field">
					<label>Customer name:</label>
					<span>{{ $order->fname }} {{ $order->lname }}</span>
				</div>

				<div class = "field">
					<label>Address:</label>
					<div>{{ $order->address }}</div>
				</div>
			</div> 

			<table cellspacing="0" width="100%">
				<thead>
					<tr>
						<th align = "left">Particulars</th>
						<th align = "center">Qty.</th>
						<th align = "right">Total</th>
					</tr>
				</thead>

				<tbody>
					@if(count($meals_ordered)>0)
						@foreach($meals_ordered as $meal_ordered)
							<tr>
								<td>
									{{ $meal_ordered->desc }}
								</td>
								<td align = "center">
									{{ $meal_ordered->qty }}
								</td>
								<td align = "right">
									{{ $meal_ordered->total }} PHP
								</td>
							</tr>
						@endforeach
						<td colspan="3" align="center"><em>***NOTHING FOLLOWS***</em></td>
					@else
						<tr>
							<td colspan="3">No orders found</td>
						</tr>
					@endif
					<tr>
						<td colspan = "3" align = "right" style = "border-top: 1px solid black;"><strong>Grand total: {{ $order->grand_total }} PHP</strong></td>
					</tr>
				</tbody>
			</table>

			<div class = "ui basic text segment form">
				<div class = "field">
					<label>Note/s:</label>
					<div>{{ $order->notes }}</div>
				</div>
			</div> 
		</div>
	</div>
@endsection

@section('scripts')
	
@endsection