@section('page-name')
	{{ config('app.name', 'Laravel') }} | Messages
@endsection

@extends('layouts.app')

@section('content')
	<div class = "row">
		<div class = "six wide column">
			<div class = "ui small raised text segment">
				<h3 class = "ui header">Send message</h3>

				<form id = "frm-chat" class = "ui small equal width form">
					<input type="hidden" id="token-chat" value="{{ csrf_token() }}">

					<div class = "field">
						<label>Username:</label>
						<input type = "text" id = "txt-chat-sender" name = "txt-chat-sender" value = "{{ Auth::user()->email }}" readonly required>
					</div>

					<div class = "field">
						<label>Message:</label>
						<textarea id = "txt-chat-content" name = "txt-chat-content" rows = "3" placeholder = "Your message here..." required></textarea> 
					</div>

					<button id = "btn-chat-send" name = "btn-chat-send" class = "ui small positive button">
						<i class = "send icon"></i>
						Send
					</button>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small raised text segment">
				<h3 class = "ui header">Messages</h3>

				{{-- <div id = "div-chat-messages" class = "ui segment field" style = "max-height: 40rem; overflow: auto;">

				</div> --}}

				<table id = "tblListOfMessages" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Sender</th>
							<th>Content</th>
							<th>Sent</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>

				<div class = "ui basic center aligned segment">
					<button class = "ui button" onclick="fnRefreshMessages()">
						<i class = "refresh icon"></i>
						Refresh
					</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$('#pgMessages').toggleClass('active');
	</script>

	<script type = "text/javascript" src = "{{ asset('js/admin/chat.js') }}"></script>
@endsection