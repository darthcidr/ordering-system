@section('page-name')
    {{ config('app.name', 'Laravel') }} | Account Settings
@endsection

@extends('layouts.app')

@section('content')
    <div class = "row">
        <div class = "column"></div>
        <div class = "nine wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Account settings</h5>

                <form class = "ui small equal width form" method = "POST" action = "{{ route('account.update',Auth::user()->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ Auth::user()->fname }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ Auth::user()->lname }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Profile picture:</label>
                        <input type = "file" accept = "image/jpeg,image/jpg,image/png" id = "picture" name = "picture" value = "{{ (Auth::user()->image) }}">
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ Auth::user()->email }}" required>
                    </div>

                    <button type = "submit" class = "ui small positive button">Update</button>
                    <button type = "reset" class = "ui small negative button">Reset fields</button>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.success('{{ session('status') }}');
        </script>
    @endif
@endsection