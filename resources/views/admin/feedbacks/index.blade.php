@section('page-name')
    {{ config('app.name', 'Laravel') }} | Feedbacks
@endsection

@extends('layouts.app')

@section('content')
    <div class = "row">        
        <div class = "column">
            <div class = "ui small raised clearing text segment">
                <h3 class = "ui header">
                    Feedbacks
                </h3>

                <table id = "tblListOfFeedbacks" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>From</th>
                            <th>Suggestion</th>
                            <th>Rating</th>
                        </tr>
                    </thead>
                </table>

                <div class = "ui basic center aligned segment">
                    <button type = "button" id = "btn-refresh-table" class = "ui small button" onclick = "$('#tblListOfFeedbacks').DataTable().ajax.reload();console.log('TABLE REFRESHED')">
                        <i class = "refresh icon"></i>
                        Reload table
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif --}}

    <script type = "text/javascript">
        // $('#pgOrders').toggleClass('active',true);
    </script>
@endsection