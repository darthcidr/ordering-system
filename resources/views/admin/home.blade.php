@section('page-name')
    {{ config('app.name', 'Laravel') }} | Admin Dashboard
@endsection

@extends('layouts.app')

@section('charts-css')
    {!! Charts::styles() !!}
@endsection

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class="ui tiny statistic">
                    <div class="label">
                        Server time
                    </div>
                    <div id = "server-time" class="value"></div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class="ui tiny statistic">
                    <div class="label">
                        Total Orders
                    </div>
                    <div class = "value">
                        <i class = "cart icon"></i>
                        {{ count($orders) }}
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class="ui tiny statistic">
                    <div class="label">
                        Registered Meals
                    </div>
                    <div class = "value">
                        <i class = "food icon"></i>
                        {{ $meals }}
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class="ui tiny horizontal statistic">
                    <div class = "value">
                        {{ round($average,1) }} <div class="ui star rating" data-max-rating = "5" data-rating="{{ round($average) }}"></div>
                    </div>

                    <div class="label">
                        Customer Rating
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small raised text segment">
                {!! $chart->html() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgHome').toggleClass('active',true);

        $(document).on('click', '#btnSalesReport', function(){
            $('[data-remodal-id=modalSalesReport]').remodal({
                closeOnOutsideClick: false
            }).open();
        });
    </script>

    {{-- <script type = "text/javascript" src = "{{ asset('js/admin/functions.js') }}"></script> --}}
@endsection

@section('charts-js')
    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
@endsection
