<div class = "ui large inverted top fixed borderless menu" style = "background-color: #005DAE; z-index: 1000">
	<a class = "header item" href = "{{ url('/') }}">
		<i class = "large empire icon"></i>
		Orders Management System
	</a>

	@if(Auth::check())
		<div class = "right menu">
			<div class = "ui simple dropdown item">
				<div class = "text">
					Welcome, {{ Auth::user()->fname }}
					<i class = "dropdown icon"></i>
				</div>
				<div class = "menu">
					<a href = "/account-settings" class = "item">
						<div class = "ui basic center aligned segment">
							@if(Auth::user()->image !== null)
								<div><img src = "{{ asset('images/profile_picture/'.Auth::user()->image) }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
							@else
								<div><img src = "{{ asset('images/profile_picture/default.jpg') }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
							@endif
							<h5 class = "ui header">
								{{ ucwords(Auth::user()->fname) }} {{ ucwords(Auth::user()->lname) }}
								<div class = "sub header">
									Admin since {{ date('Y',strtotime(Auth::user()->created_at)) }}
								</div>
							</h5>
						</div>
					</a>
					<a href = "{{ route('logout') }}" id = "btnLogout" class = "item">Logout</a>
				</div>
			</div>
		</div>
	@endif
</div>

@section('scripts')
	
@endsection