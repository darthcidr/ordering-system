<div class = "ui visible inverted vertical borderless sidebar menu" style = "padding-top: 3.5rem; font-size: 12px;">
	<a id = "pgHome" class = "item" href = "/home">Home</a>
	{{-- <a id = "pgOrders" class = "item" href = "/orders">
		Orders
		<span id = "unclaimed-orders-count" class = "ui mini hidden red label"></span>
	</a> --}}
	<div class = "item">
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				Orders
				<i class = "dropdown icon"></i>
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a class = "item" href = "/orders/unclaimed">
						Unclaimed
						<span id = "unclaimed-orders-count" class = "ui mini hidden red label"></span>
					</a>
					<a class = "item" href = "/orders/all">
						All
						<span id = "all-orders-count" class = "ui mini hidden red label"></span>
					</a>
				</ul>
			</div>
		</div>
	</div>
	<a id = "pgMeals" class = "item" href = "/meals">Meals</a>
	<a id = "pgMessages" class = "item" href = "/messages">
		Messages
		<span id = "messages-count" class = "ui mini hidden red label"></span>
	</a>
	<a id = "pgFeedbacks" class = "item" href = "/feedbacks">Feedbacks</a>
	<div class = "item">
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				Reports
				<i class = "dropdown icon"></i>
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a class = "item" href = "/reports/list-of-orders" target = "_new">Orders</a>
					<a class = "item" href = "/reports/list-of-customers" target = "_new">Customers</a>
					<a id = "btnSalesReport" class = "item">Sales</a>
				</ul>
			</div>
		</div>
	</div>
	<a id = "pgSettings" class = "item" href = "/settings">System settings</a>
</div>

<div class="remodal" data-remodal-id="modalSalesReport">
	<button data-remodal-action="close" class="remodal-close"></button>
	
	<h4 class = "ui header">
		Select sales year
	</h4>
	
	<form id = "frmSelectSalesYear" class = "ui small equal width form" method = "POST" action = "/reports/sales" target="_new">
		{{ csrf_field() }}

		<div class = "field">
			<label>Year</label>
			<select id = "cmbSalesYear" name = "cmbSalesYear" class = "ui small fluid search dropdown" required>
				@for($i = date('Y') ; $i > 2016 ; $i--)
					{!! '<option value = "'.$i.'">'.$i.'</option>' !!}
				@endfor
			</select>
		</div>

		<button type = "submit" class = "ui small positive button"><i class = "send icon"></i>Launch report</button>
	</form>
</div>