<div class = "ui large top fixed secondary pointing menu" style = "background-color: #fff;">
	@if(!Auth::check())
		<a class = "header item" href = "{{ url('/') }}">
			<i class = "large empire icon"></i>
			ORDERS MANAGEMENT SYSTEM
		</a>

		<div class = "right menu">
			<a id = "pgLogin" class = "item" href = "{{ route('login') }}">Login</a>
			<a id = "pgRegister" class = "item" href = "{{ route('register') }}">Register</a>
		</div>
	@else
		<a class = "header item" href = "{{ url('/') }}">
			<i class = "large empire icon"></i>
		</a>
		<a id = "pgHome" class = "item" href = "/tnm-admin/home">Home</a>
		<a id = "pgOrders" class = "item" href = "/orders">
			Orders
			<span id = "unclaimed-orders" class = "ui hidden red label"></span>
		</a>
		<a id = "pgMeals" class = "item" href = "/meals">Meals</a>
		<a id = "pgMessages" class = "item" href = "/messages">Messages</a>
		<div class = "ui simple dropdown item">
			<div class = "text">
				Reports
				<i class = "dropdown icon"></i>
			</div>
			<div class = "menu">
				<a class = "item" href = "/reports/list-of-orders" target = "_new">Orders</a>
				<a class = "item" href = "/reports/list-of-customers" target = "_new">Customers</a>
				<a class = "item" href = "/reports/sales" target = "_new">Sales</a>
			</div>
		</div>

		<div class = "right menu">
			<div class = "item">
				Welcome, {{ Auth::user()->fname }}
			</div>
			<a href = "{{ url('/logout') }}" id = "btnLogout" class = "item">Logout</a>
		</div>
	@endif
</div>

@section('scripts')
	
@endsection