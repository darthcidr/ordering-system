<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<link type = "text/css" rel = "stylesheet" href = "{{ asset('packages/packages.css') }}">
		

		<style>
			#banner {
				background-color: #000;
				height: 50rem;				
			}
			#brand {
				font-size: 48px;
				position: relative;
				top: 15rem;
				/*left: 8rem;*/

				text-align: center;
			}
			#tagline {
				font-size: 24px;
			}
			#div-content,#div-marketing,#footer {
				padding-top: 3rem;
			}
			#footer {
				font-size: 12px;
			}
			p {
				text-align: justify;
			}
			.horizontal.list {
				text-align: center;
				font-size: 72px;
			}
		</style>

		<title>{{ config('app.name', 'Laravel') }}</title>
	</head>

	<body>
		<div id = "banner" class = "ui fluid container">
			<div class = "ui basic clearing segment">
				<div id = "brand" class = "ui inverted header">
					Tapa ni Mau
					<div id = "tagline" class = "sub header">
						<em>Ang sagot sa iyong post-inuman cravings!</em>
					</div>

					<a href = "#div-order-form" role = "button" class = "ui green button">
						<i class = "cart icon"></i>
						Order now!
					</a>

					<a href = "/chat" id = "btn-set-username" role = "button" class = "ui primary button">
						<i class = "comments icon"></i>
						Chat with us!
					</a>

					<a href = "/orders/search-form" role = "button" class = "ui positive button">
						<i class = "search icon"></i>
						Search
					</a>
				</div>
			</div>
		</div>

		<div id = "div-marketing" class = "ui container">
			<div class = "ui center aligned stackable equal width grid">
				<div class = "row">
					<div class = "column">
						<i class = "huge smile icon"></i>
						<div class = "ui header">Satisfaction</div>
						<div class = "description">Our meals are top of the line&mdash;guaranteed to make our customers come back time and time again.</div>
					</div>
					
					<div class = "column">
						<i class = "huge thumbs up icon"></i>
						<div class = "ui header">Proven and tested</div>
						<div class = "description">Sober or drunk, our customers say only one thing&mdash;<em>ang sarap ng pagkain dito!</em></div>
					</div>
					
					<div class = "column">
						<i class = "huge quote left icon"></i>
						<div class = "ui header">Talk of the town</div>
						<div class = "description">Once tasted, our customers do the word-spreading and advertising for us.</div>
					</div>
				</div>
			</div>
		</div>

		<div id = "div-content" class = "ui container">
			<div class = "ui stackable equal width grid">
				<div class = "row">
					<div class = "column">
						<div id = "div-order-form" class = "ui small raised clearing text segment">
							<h4 class = "ui header">
								<i class = "cart icon"></i>
								Order form
							</h4>

							<table id = "tblMealsToOrder" class = "ui celled striped form table" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Meal</th>
										<th>Quantity</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>

								</tbody>

								<tfoot>
									<tr>
										<td></td>
										<td>
											<select id = "cmb-order-meals" name = "cmb-order-meals" class = "ui small fluid search dropdown" required>
									
											</select>
										</td>
										<td>
											<input type = "number" id = "txt-order-qty" name = "txt-order-qty" value = "1" step = "1" min = "1" required>
										</td>
										<td>
											<button type = "button" id = "btnAddMealToOrder" class = "ui mini primary button">
												<i class = "plus icon"></i>
												Add to cart
											</button>
										</td>
									</tr>
								</tfoot>
							</table>

							<form id = "frm-order" class = "ui small equal width form">
								{{-- {{ csrf_field() }} --}}
								<input type="hidden" id="token" value="{{ csrf_token() }}">

								<div class = "required fields">
									<div class = "field">
										<label>First name</label>
										<input type = "text" id = "txt-order-fname" name = "txt-order-fname" required>
									</div>

									<div class = "field">
										<label>Last name:</label>
										<input type = "text" id = "txt-order-lname" name = "txt-order-lname" required>
									</div>

									<div class = "field">
										<label>Contact number</label>
										<div class = "ui labeled input">
											<div class = "ui label">+63</div>
											<input type = "text" id = "txt-order-contact-number" name = "txt-order-contact-number" data-mask = "9999999999" required>
										</div>
									</div>
								</div>

								<div class = "required field">
									<label>Address</label>
									<textarea id = "txt-order-address" name = "txt-order-address" rows = "2" required></textarea>
								</div>


								<div class = "required field">
									<label>Note/s</label>
									<textarea id = "txt-order-notes" name = "txt-order-notes" rows = "2"></textarea>
								</div>

								<div class = "field">
									<label>Grand total</label>
									<input type = "number" id = "nud-grand-total" name = "nud-grand-total" min = "0" readonly>
								</div>

								<div class = "field">
									<button type = "submit" id = "btn-order" name = "btn-order" class = "ui small green button">
										<i class = "send icon"></i>
										Place order
									</button>
								</div>

								<div class = "field">
									<div class = "sub header">
										<em>* Field required</em>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

				<div class="remodal" data-remodal-id="div-feedback">
					<button data-remodal-action="close" class="remodal-close"></button>
					
					<h4 class = "ui header">
						<i class = "comment icon"></i>
						We'd love to hear more from you!
					</h4>
					
					<form id = "frmAddFeedback" class = "ui small equal width form" method = "POST" action = "{{ route('feedbacks.store') }}">
						{{ csrf_field() }}

						<div class = "required field">
							<label>How satisfied were you?</label>
							<div id = "satisfaction-rating" class="ui large heart rating" data-rating="1" data-max-rating = "5"></div>
							<input type = "hidden" id = "feedback-satisfaction-rating" name = "feedback-satisfaction-rating">
						</div>

						<div class = "field">
							<label>Comment:</label>
							<textarea id = "feedback-suggestions" name = "feedback-suggestions" rows = "3"></textarea>
						</div>

						<div class = "required field">
							<label>Email:</label>
							<input type = "email" id = "feedback-email" name = "feedback-email" required>
						</div>

						<button type = "submit" class = "ui small positive button"><i class = "send icon"></i>Submit</button>
					</form>
				</div>
			</div>
		</div>

		<footer id = "footer" class = "ui bottom secondary stackable menu">
			<div class = "item">
				<i class = "phone icon"></i>
				+63-912-345-6789
			</div>

			<div class = "item">
				<i class = "mail icon"></i>
				<a href = "mailto:{{ 'darthcaedus78@gmail.com' }}" target = "_new">{{ 'darthcaedus78@gmail.com' }}</a>
			</div>

			<div class = "item">
				<i class = "github icon"></i>
				<a href = "https://github.com/darthjarcas" target = "_new">darthjarcas</a>
			</div>

			<div class = "item">
				All rights reserved &reg;
			</div>

			<a class = "item" id = "modal-feedback">
				<i class = "chat icon"></i>
				Got suggestions?
			</a>

			<div class = "right menu">
				<a href = "#banner" class = "item">
					<i class = "arrow up icon"></i>
					Back to top
				</a>
			</div>
		</footer>

		<script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
		<script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
		<script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
		<script src="{{ asset('packages/Remodal/dist/remodal.js') }}"></script>
		<script src="{{ asset('packages/jquery-mask/dist/jquery.mask.js') }}"></script>
		{{-- <script src="{{ asset('packages/nanobar/nanobar.js') }}"></script> --}}
		{{-- <script src="{{ asset('packages/pace/pace.min.js') }}"></script> --}}

		<script src="{{ asset('js/core.js') }}"></script>

		@if(session()->has('status'))
			<script type = "text/javascript">
				toastr.success('{{ session('status') }}');
			</script>
		@endif

		{{-- @if(session()->has('info'))
			<script type = "text/javascript">
				toastr.info('{{ session('info') }}');
			</script>
		@endif --}}
	</body>
</html>